﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
public class Enemy : MonoBehaviour,IDamageable {
	 float currethHealthPoints=100F;
	[SerializeField] float maxHealthPoints=100F;
	[SerializeField] float attackRadius=4F;
	[SerializeField] float moveRadius=8F;
	[SerializeField] float damagePerShot=2F;
	[SerializeField] float ShotsperSecond=1.5F;
	[SerializeField] Vector3 AimOffset= new Vector3(0F,15F,0F);
	[SerializeField] GameObject projectiletoUse;
	[SerializeField] GameObject projectileSocket;

	AICharacterControl  aiCharacterControl=null;
	GameObject player=null;
	private  bool isAttacking = false;
	public float healthAsPercentage {
		get
		{
			return currethHealthPoints/ maxHealthPoints	;
		}
	}
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		aiCharacterControl = GetComponent<AICharacterControl> ();
	}

	void SpawnProjectile ()
	{
		GameObject gm = (GameObject)Instantiate (projectiletoUse, projectileSocket.transform.position, Quaternion.identity);
		Projectile p = gm.GetComponent<Projectile> ();
		p.damageCoused = damagePerShot;
		Vector3 unitVectorToPlayer = (player.transform.position+AimOffset - projectileSocket.transform.position).normalized;
		gm.GetComponent<Rigidbody> ().velocity = unitVectorToPlayer * p.projectilespeed;
	}
	
	// Update is called once per frame
	void Update () {
		float distanceToPlayer = Vector3.Distance (player.transform.position, transform.position);
		if (distanceToPlayer <= moveRadius) {
			aiCharacterControl.SetTarget (player.transform);
		} else {
			aiCharacterControl.SetTarget (transform);
		}

		if (distanceToPlayer <= attackRadius&& !isAttacking) {
			isAttacking = true;
			InvokeRepeating ("SpawnProjectile", 0F, ShotsperSecond);
//			SpawnProjectile ();


		} else if( distanceToPlayer > attackRadius ) {
			isAttacking = false;
			CancelInvoke ();
		}

	
	}
	IEnumerator CoroutineFireStart()
	{
		
		yield return new WaitForSeconds (ShotsperSecond);
		SpawnProjectile ();

	}

	void OnDrawGizmos()
	{
		Gizmos.color = new Color (255F, 0F, 0F, .5F);
		Gizmos.DrawWireSphere (transform.position, attackRadius);
		Gizmos.color = new Color (0F, 255F, 255F, .5F);
		Gizmos.DrawWireSphere (transform.position, moveRadius);
	}

	#region IDamageable implementation

	public void TakeDamage (float damage)
	{
		currethHealthPoints = Mathf.Clamp (currethHealthPoints - damage, 0F, maxHealthPoints);
		if (currethHealthPoints <= 0)
			Destroy (gameObject);
	}

	#endregion
}
