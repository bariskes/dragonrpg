﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour,IDamageable {

	[SerializeField] int enemyLayer=9;
	 float currethHealthPoints=100F;
	[SerializeField] float maxHealthPoints=100F;
	[SerializeField] float damagePerHit=20F;
	[SerializeField] float minTimeBetweenHits=0.5F;
	[SerializeField] float maxAttackRange=2F;
	float lastHitTime=0F;
	CameraRaycaster cameraRayCaster;
	GameObject currentTarget;
	public float healthAsPercentage {
		get
		{
			return currethHealthPoints/ maxHealthPoints	;
		}
	}
	// Use this for initialization
	void Start () {
		cameraRayCaster = FindObjectOfType<CameraRaycaster> ();
		cameraRayCaster.notifyMouseClickObservers += OnMouseClick;
	}
	void OnMouseClick(RaycastHit raycastHit, int layerHit)
	{
		if (layerHit == enemyLayer) {
			var enemy = raycastHit.collider.gameObject;

			var enemyComponent = enemy.GetComponent<Enemy> ();
			// check enemy is in range
			if ((enemy.transform.position - transform.position).magnitude > maxAttackRange) {
				return;
				
			}
			currentTarget = enemy;
			if (Time.time - lastHitTime > minTimeBetweenHits) {
				if (enemyComponent != null) {
					enemyComponent.TakeDamage (damagePerHit);
					lastHitTime = Time.time;
				}
			}


		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	#region IDamageable implementation
	public void TakeDamage (float damage)
	{
		currethHealthPoints = Mathf.Clamp (currethHealthPoints - damage, 0F, maxHealthPoints);
			}			#endregion
}
