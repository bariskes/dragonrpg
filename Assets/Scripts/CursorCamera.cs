﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorCamera : MonoBehaviour {

	[SerializeField]
	Texture2D walkCursor=null;
	[SerializeField]
	Vector2 cursorHotSpot=new Vector2(96,96);

	[SerializeField]
	Texture2D attackCursor=null;
	[SerializeField]
	Texture2D unknowCursor=null;
	CameraRaycaster cmr;
	[SerializeField] const int walkableLayerNumber=8;
	[SerializeField]  const int enemyLayerNumber=9;

	// Use this for initialization
	void Start () {
		cmr=GetComponent<CameraRaycaster> ();
		cmr.notifyLayerChangeObservers += OnLayerChanged;


	}
	
	// Update is called once per frame
	void OnLayerChanged (int newLayer) // only called when layer changes{
	{
		//print ("Cursor Affordance delegate " + cmr.layerHit);
		switch (newLayer) {
		case walkableLayerNumber:
			Cursor.SetCursor (walkCursor, cursorHotSpot, CursorMode.ForceSoftware);
			break;
		case enemyLayerNumber:
			Cursor.SetCursor (attackCursor, cursorHotSpot, CursorMode.ForceSoftware);
			break;
			default:
			Cursor.SetCursor (unknowCursor, cursorHotSpot, CursorMode.ForceSoftware);
			break;
		}


	
		
		
	}
}
