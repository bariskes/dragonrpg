﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	[SerializeField] public float projectilespeed=30F;
	[SerializeField] public float damageCoused=10F;
	void OnTriggerEnter(Collider collider)
	{
	//	print ("Projectile Hit" + collider.gameObject);
		Component damageableComponent = collider.gameObject.GetComponent (typeof(IDamageable));
	//	print ("Projectile Hit" + collider.gameObject+ " damageableComponent " + damageableComponent);
		if (damageableComponent) {
			(damageableComponent as IDamageable).TakeDamage (damageCoused);

		}


	}
}
