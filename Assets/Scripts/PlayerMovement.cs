using System;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.AI;
using UnityEngine;

[RequireComponent(typeof (ThirdPersonCharacter))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(AICharacterControl))]
public class PlayerMovement : MonoBehaviour
{
	public bool isInDirectMode = false;
    ThirdPersonCharacter m_Character=null;   // A reference to the ThirdPersonCharacter on the object
    CameraRaycaster cameraRaycaster=null;
    Vector3 currentClickTarget;
	AICharacterControl aiCharacterControl=null;
//	[SerializeField] float walkMoveStopRadius=0.2f;
//	[SerializeField] float attackMoveStopRadius=5f;
	[SerializeField] const int walkableLayerNumber=8;
	[SerializeField]  const int enemyLayerNumber=9;
	GameObject walkTarget=null;
     void Start()
    {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        m_Character = GetComponent<ThirdPersonCharacter>();
		aiCharacterControl = GetComponent<AICharacterControl>();
        currentClickTarget = transform.position;
		cameraRaycaster.notifyMouseClickObservers +=	ProcessMouseClick;
		walkTarget = new GameObject ("walkTarget");
    }
	void ProcessMouseClick(RaycastHit raycasthit,int layerHit )
	{
		switch (layerHit) {
		case enemyLayerNumber:
			// navigate to enemy
			GameObject enemy = raycasthit.collider.gameObject;
			aiCharacterControl.SetTarget (enemy.transform);


			break;
		case walkableLayerNumber:
			// navigate to point ground 
			walkTarget.transform.position = raycasthit.point;
			aiCharacterControl.SetTarget (walkTarget.transform);
			break;
		default:
			Debug.Log ("Unknow Mouse Click");
			break;
			
		}
	}
    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
		if (Input.GetKeyDown (KeyCode.G)) {
			isInDirectMode = !isInDirectMode;
			currentClickTarget = transform.position;

		}


		if (isInDirectMode) {
			GamePadMovement ();
		} else {
			MouseMovement ();
		}
    }
	void MouseMovement()
		{
		
	}
	void GamePadMovement()
	{
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");

		Vector3 m_CamForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
		Vector3 m_Move = v*m_CamForward + h*Camera.main.transform.right;
		m_Character.Move (m_Move, false, false);

	
	}
//	void OnDrawGizmos()
//	{
//		Gizmos.color = Color.black;
//		Gizmos.DrawLine (transform.position,currentClickTarget);
//		Gizmos.DrawSphere (currentClickTarget, 0.1F);
////		Gizmos.DrawWireSphere (transform.position, walkMoveStopRadius);
//	//	Gizmos.color = new Color(155F,0,0,0.5F);
//	//	Gizmos.DrawSphere (transform.position, attackMoveStopRadius);
//
//	}
//	void MouseMovement()
//	{
//		if (Input.GetMouseButton(0))
//		{
//			
//			switch (cameraRaycaster.layerHit) {
//			case Layer.Walkable:
//				currentClickTarget = cameraRaycaster.hit.point;
//
//				break;
//			case Layer.Enemy:
//				print ("Not Moving");
//				break;
//			default:
//				break;
//			}
//		}
//		var playerToclickPoint =currentClickTarget- transform.position ;
//		if (playerToclickPoint.magnitude >= walkMoveStopRadius) {
//			m_Character.Move (playerToclickPoint, false, false);	
//		} else {
//			m_Character.Move (Vector3.zero, false, false);
//		}
//	}
}

